<?php

use Psr\Http\Message\ResponseInterface as Response;
use Psr\Http\Message\ServerRequestInterface as Request;
use Slim\Factory\AppFactory;

/* require_once */
require_once(__DIR__.'/../ext/vendor/autoload.php');
require_once(__DIR__.'/../lib/controller/ArticleCnt.php');

/* Creamos la aplicacion Slim a traves del factory */
$app = AppFactory::create();
$app->addRoutingMiddleware();

/* Configuracion de la respuesta para requests GET con path /article 
 * Esta request devuelve un listado los registros disponibles en la base de datos.
 */
$app->get('/article',function( Request $request, Response $response, array $args) {
    $cnt = new ArticleCnt();
    $articles = $cnt->articleList();

    $arrayArt = array();
    foreach ($articles as $art) {
        $arrayArt[] = $art->toArray();
    }

    $newArt = $response->withHeader('Content-type', 'application/json');
    $newArt->getBody()->write(json_encode($arrayArt));
    return $newArt;
});

/* Configuracion de la respuesta para requests GET con path /article/idweapon 
 * Esta request devuelve el detalle del registro con la id definida en la URL de la 
 * peticion.
 */
$app->get('/article/{artid}',function(Request $request, Response $response, array $args) {
  $artid = $args['artid'];
  $cnt = new ArticleCnt();

  $artobj = $cnt->detailArticle($artid);

  $newArt = $response->withHeader('Content-type', 'application/json');
  $newArt->getBody()->write(json_encode($artobj->toArray()));
  return $newArt;
});

/* Configuracion de la respuesta para requests POST con el path /article 
 * Esta request crea un nuevo registro con los datos que llegan en el cuerpo de 
 * la peticion.
 */
$app->post('/article',function(Request $request, Response $response, array $args) {
    $artjson = $request->getParsedBody();
    $cnt = new ArticleCnt();

    $newarticle = $cnt->addArticle($artjson['tipo'], $artjson['nombre'],
        $artjson['material'], $artjson['origen'], $artjson['peso'],
        $artjson['filo'], $artjson['media']);

    $newArt = $response->withHeader('Content-type', 'application/json');
    $newArt->getBody()->write(json_encode($newarticle->toArray()));
    return $newArt;
});

/* Configuracion de la respuesta para requests DELETE con path /article/idweapon 
 * Esta request elimina el registro con la id definida en la URL de la peticion y devuelve 
 * los datos que habia en el registro.
 */
$app->delete('/article/{artid}',function(Request $request, Response $response, array $args) {
  $artid = $args['artid'];
  $cnt = new ArticleCnt();

  $artobj = $cnt->removeArticle($artid);

  $newArt = $response->withHeader('Content-type', 'application/json');
  $newArt->getBody()->write(json_encode($artobj->toArray()));
  return $newArt;
});

/* Configuracion de la respuesta para requests PUT con path /article/artid 
 * Esta request modifica el registro con la id definida en la URL de la peticion con los datos 
 * que llegan en el cuerpo de la peticion y devuelve el resultado de la actualizacion.
 */
$app->put('/article/{artid}',function(Request $request, Response $response, array $args) {
  $artid = $args['artid'];
  $artjson = $request->getParsedBody();
  $cnt = new ArticleCnt();

  $newarticle = $cnt->addArticle($artjson['title'], $artjson['description'],
      $artjson['date'], $artjson['author'], $artjson['phylosophy'],
      $artjson['media'], $artjson['mediatype']);

  $artobj = $cnt->removeArticle($artid);

  $newArt = $response->withHeader('Content-type', 'application/json');
  $newArt->getBody()->write(json_encode($artobj->toArray()));
  return $newArt;
});

/* Ejecutamos la aplicacion creada una vez configuradas todas las duplas 
 * verbo HTTP / path
 */
$app->run();
