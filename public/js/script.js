
/* 
 * Esta funcion esconde los elementos del formulario de modificacion.
 */
function noUpdate(){
  $('#updateButton').hide();
  $('#createformart').hide();
}

/* 
 * Esta funcion muestra el campo multimedia en funcion del tipo de registro.
 */
function hideShowMedia(){
  var typeArt = $('#arttype').val();
  if(typeArt == 0){
	  $('.marticle').hide();
  }else if(typeArt == 1){
	  $('.marticle').show();
	  $('#mediatitle').html('Imagen');
  }else if(typeArt == 2){
	  $('.marticle').show();
	  $('#mediatitle').html('YouTube Id');
  }else if(typeArt == 3){
	  $('.marticle').show();
	  $('#mediatitle').html('SoundCloud Id');
  }
}

/*
 * Esta funcion envia una peticion al servidor para obtener el listado
 * de registros y los muestra en contenedor HTML con ID wlist.
 */
function listArticles(){
	$.ajax({
		url:"/article",
		method:"get",
		accept:"json",
		dataType: "json"
	}).done(function(response){
		var htmlart = '';
		$.each(response, function(index,value){
			htmlart += '<div>';
			htmlart += '<h3><span>'+value.title+'</span></h3>';
			htmlart += '<p>Description: '+value.description+'</p>';
			htmlart += '<p>Date: '+value.date+'</p>';
			htmlart += '<p>Author: '+value.author+'</p>';
			htmlart += '<p>Pylosophy: '+value.phylosophy+'</p>';
			htmlart += '<p><span onclick="detailsArt(\''+value.artid+'\')">View</span></br>';
			htmlart += '<span onclick="updateFormArt(\''+value.artid+'\')">Update</span></br>';
			htmlart += '<span onclick="deleteArt(\''+value.artid+'\')">Delete</span></p>';
			htmlart += '</div>';
		});
	$('#artlist').html(htmlart);

	});
}

/*
 * Esta funcion envia una peticion al servidor para obtener el detalle
 * de un registro y los muestra en contenedor HTML con ID wdetail.
 */
function detailsArt(article){
	$.ajax({
		url:"/article/"+article,
		method:"get",
		accept:"json",
		dataType: "json"
	}).done(function(response){
		var htmlart = '';
			htmlart += '<div>';
			htmlart += '<h3><span>'+response.title+'</span></h3>';
			htmlart += '<p>Description: '+value.description+'</p>';
			htmlart += '<p>Date: '+value.date+'</p>';
			htmlart += '<p>Author: '+value.author+'</p>';
			htmlart += '<p>Pylosophy: '+value.phylosophy+'</p>';
			if(response.tipo == 1){
				htmlart += '<p><img src="'+response.media1+'"/></p>';
			}else if(response.tipo == 2){
				htmlart += '<iframe width="560" height="315" src="https://www.youtube.com/embed/'+response.media2+'" frameborder="0" allow="accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>';
			}else if(response.tipo == 3){
				//htmlart += '<p><img src="'+response.media1+'"/></p>';
			}
			htmlart += '</div>';
	$('#artdetail').html(htmlart);
	});
}

/*
 * Esta funcion envia una peticion al servidor para eliminar el
 * registro con la id definida en la URL y refresca el listado de registros.
 */
function deleteArt(article){
	$.ajax({
		url:"/article/"+article,
		method:"delete",
		accept:"json",
		dataType: "json"
	}).done(function(response){
		listWeapons();
	});
}

/*
 * Esta funcion carga los campos del registro a modificar en el formulario 
 * disponible en la pagina principal.
 */
function updateFormArt(article){
  $('#createButton').hide();
  $('#updateButton').show();
  $('#createformart').show();
  $.ajax({
		url:"/article/"+article,
		method:"get",
		accept:"json",
		dataType: "json"
	}).done(function(response){
    $("#arttitle").val(response.title);
    $("#artdesc").val(response.description);
    $("#artdate").val(response.date);
    $("#artaut").val(response.author);
    $("#artpylo").val(response.phylosophy);
    hideShowMedia();
    if(response.tipo == 1){
      $("#artmedia").val(response.media1);
    }else if(response.tipo == 2){
  	  $("#artmedia").val(response.media2);
    }else if(response.tipo == 3){
  	  $("#artmedia").val(response.media3);
    }
    $("#artidup").val(response.artid);
	});

}

/*
 * Esta funcion envia una peticion al servidor para modificar el
 * registro con la id definida en la URL y los datos en el cuerpo
 * de la peticion y refresca el listado de registros.
 */
function updateArticle(){
	var article = { "title" : $("#wfname").val(),
			"description" : $("#wffil").val(),
			"date" : $("#wforig").val(),
			"author" : $("#wfmat").val(),
			"phylosophy" : $("#wfweig").val(),
			"tipo" : $("#arttype").val(),
			"media" : $("#wfmedia").val(),
			};
  var artid = $("#artidup").val();

	$.ajax({
		url:"/article/"+artid,
		method:"put",
		accept:"json",
		data : article,
		dataType: "json"
	}).done(function(response){
		updateFormArt();
		updateArticles();
	});
}

/*
 * Esta funcion limpia los campos del formulario para convertirlo
 * en un formulario de creacion de registros en lugar de uno de 
 * modificacion.
 */
function createFormArt(){
  $("#arttitle").val('');
  $("#artdesc").val('');
  $("#artdate").val('');
  $("#artaut").val('');
  $("#artpylo").val('');
  hideShowMedia();
  $("#articmedia").val('');
  $("#artidup").val('');
  $('#createButton').show();
  $('#updateButton').hide();
  $('#createformart').hide();
}

/*
 * Esta funcion envia una peticion al servidor para crear un
 * nuevo registro con los datos en el cuerpo de la peticion 
 * y refresca el listado de registros.
 */
function createArticle(){
	var article = { "title" : $("#arttitle").val(),
			"description" : $("#artdesc").val(),
			"date" : $("#artdate").val(),
			"author" : $("#artaut").val(),
			"phylosophy" : $("#artpylo").val(),
			"mediatype" : $("#arttype").val(),
			"media" : $("#articmedia").val()
			};

	$.ajax({
		url:"/article",
		method:"post",
		accept:"json",
		data : article,
		dataType: "json"
	}).done(function(response){
		createFormArt();
		listArticles();
	});
}
