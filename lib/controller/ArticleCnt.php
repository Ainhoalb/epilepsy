<?php

require_once(__DIR__.'/../model/db/ArticleDb.php');

class ArticleCnt{
    
    public function articleList(){
        $db = new ArticleDb();
        return $db->listArticle();
    }
    
    public function listArticleById($id) {
        $db = new ArticleDb();
        return $db -> getArticleById($id);
    }
    
    public function detailArticle($id){
        $db = new ArticleDb();
        return $db->getArticleById($id);
    }
    
    public function removeArticle($id){
        $db = new ArticleDb();
        return $db->deleteArticle($id);
    }
    
    public function addArticle($atitle, $adescription, $adate, $aauthor, $aphylosophy, $amedia,$amediatype){
        $db = new ArticleDb();
        return $db->insertArticle($atitle, $adescription, $adate, $aauthor, $aphylosophy,$amedia,$amediatype);
    }
    
    public function updateArticle($aid,$atitle, $adescription, $adate, $aauthor, $aphylosophy, $amedia,$amediatype){
        $db = new ArticleDb();
        return $db->updateArticle($aid,$atitle, $adescription, $adate, $aauthor, $aphylosophy,$amedia,$amediatype);
        
    }
    
    public function defaultArticle(){
        $cnt = new ArticleDb();
        $defArt = array();
        
        $defArt[] = $cnt->insertArticle("Default Article", "This is my default article", "2019/12/08", "Ainhoaa", "Phylo","","");
        
        return $defArt;
    }
}