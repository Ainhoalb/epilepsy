<?php 

require_once(__DIR__.'/../Article.php');

class ArticleDb{
    
  public function listArticle(){
      
      //we declare a mongo collection
      $collection = (new MongoDB\Client)->epilepsyapp->article;
      $cursor = $collection->find([]);
    
      
      $art = array();
      foreach ($cursor as $mongoarticle){
          
          $artid = (string)$mongoarticle["_id"];
          $arttitle = $mongoarticle['title'];
          $artdescription = $mongoarticle['description'];
          $artdate = $mongoarticle['date'];
          $artauthor = $mongoarticle['author'];
          $artphylosophy = $mongoarticle['phylosophy'];
          $artmedia = $mongoarticle['media'];
          $artmediatype = $mongoarticle['mediatype'];
          
          $article = new Article($artid,$arttitle, $artdescription, $artdate, $artauthor, $artphylosophy,$artmedia,$artmediatype);
          
          array_push($art, $article);
      }
      
      return $art;
  }
  
  public function getArticleById($artid){
      
      $collection = (new MongoDB\Client)->epilepsyapp->article;
      $info = $collection->findOne(["_id" => new MongoDB\BSON\ObjectId($artid)]);
      
      $article = array('id' => $info["_id"]->__toString(), 'title' => $info['title'], 'description' => $info['description'], 'date' => $info['date'],'author' => $info['author'], 'phylosophy' => $info['phylosophy'],
          'media' => $info['media'], 'mediatype'=> $info['mediatype']);
      
      $article = new Article($article['id'],$article['title'],$article['description'],$article['date'],$article['author'],$article['phylosophy'],$article['media'],$article['mediatype']);
      
      return $article;
  }
  
  public function insertArticle($title,$description,$date,$author, $phylosophy,$media,$mediatype){
      
      $collection = (new MongoDB\Client)->epilepsyapp->article;
      
      $article = $this->toArray($title, $description,$date, $author,$phylosophy,$media,$mediatype);
      
      $insertOneResult = $collection->insertOne($article);
      
      return $insertOneResult->getInsertedId()->__toString();
  }
  
  public function deleteArticle($aid){
      
      $art = $this->getArticleById($aid);
      
      $collection = (new MongoDB\Client)->epilepsyapp->article;
      
      $deleteOneResul = $collection->deleteOne(["_id" => new MongoDB\BSON\ObjectId($aid)]);
      
      return $art;
  }
  
  public function updateArticle($artid,$arttitle,$artdescription,$artdate,$artauthor, $artphylosophy, $artmedia, $artmediatype){
      
      $collection = (new MongoDB\Client)->epilepsyapp->article;
      
      $art = $this->toArray($arttitle,$artdescription,$artdate,$artauthor, $artphylosophy, $artmedia, $artmediatype);
      
      $updateOneResult = $collection->updateOne(['_id' => new \MongoDB\BSON\ObjectID($artid)], ['$set' => $art]);
      
      return $this->getArticleById($artid);
  }
  
  public function toArray($title,$description,$date,$author, $phylosophy,$media, $mediatype){
      $info = array();
      
      $info["title"] = $title;
      $info["description"] = $description;
      $info["date"] = $date;
      $info["author"] = $author;
      $info["phylosophy"] = $phylosophy;
      $info["media"] = $media;
      $info["mediatype"] = $mediatype;
      
      return $info;
  }
  
  public function toJSON(){
      $infoson = $this->toArray();
      return json_encode($infoson);
  }
   
}