<?php 

require_once(__DIR__.'/Article.php');

class ArticleImg extends Article{
    
    private $_image;
    
    public function __construct($tit, $desc, $dat, $aut, $pylo, $mediat, $aid = null){
      
        $this->setTitle($tit);
        $this->setDescription($desc);
        $this->setDate($dat);
        $this->setAuthor($aut);
        $this->setPhylosophy($pylo);
        $this->setMediaType($mediat);
        $this->setId($aid);
    }
    
    public function getType(){
        return W_TYPE_IMG;
    }
    
    public function getImage(){
        return $this->_image;
    }

    public function setImage($_image){
        $this->_image = $_image;
    }

    public function getView(){
        return __DIR__.'/../inc/ArticleImg.php';
    }
    
   
    public function toArray(){
        $obj = array();
        $obj['title'] = $this->getTitle();
        $obj['description'] = $this->getDescription();
        $obj['date'] = $this->getDate();
        $obj['author'] = $this->getAuthor();
        $obj['phylosophy'] = $this->getPhylosophy();
        $obj['media1'] = $this->getImagen();
        $obj['mediatype'] = W_TYPE_IMG;
        if($this->getWid() != NULL){
            $obj['aid'] = $this->getAid();
        }
        
        return $obj;
    }
    
}