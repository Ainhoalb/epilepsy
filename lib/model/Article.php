<?php

class Article{
    
    private $_aid;
    public $_atitle;
    private $_adescription;
    private $_adate;
    private $_aauthor;
    private $_aphylosophy;
    
    public function __construct($aid = null,$atitle = "", $adescription = "", $adate="", $aauthor="", $aphylosophy="",$amedia="",$amediatype=""){
        $this->setId($aid);
        $this->setTitle($atitle);
        $this->setDescription($adescription);
        $this->setDate($adate);
        $this->setAuthor($aauthor);
        $this->setPhylosophy($aphylosophy);
    }
    
    public function getTitle(){
        return $this->_atitle;
    }
    
    public function getDescription(){
        return $this->_adescription;
    }
    
    public function  getDate(){
        return $this->_adate;
    }
    
    public function  getAuthor(){
        return $this->_aauthor;
    }
    
    public function getPhylosophy(){
        return $this->_aphylosophy;
    }
    
    public function getAid(){
        return $this->_aid;
    }
    
    public function setId($aid){
        $this->_aid = $aid;
    }
    
    public function setTitle($atitle){
        $this->_atitle = $atitle;
    }
    
    public function setDescription($adescription){
        $this->_adescription = $adescription;
    }
    
    public function  setDate($adate){
        $this->_adate = $adate;
    }
    
    public function  setAuthor($aauthor){
        $this->_aauthor = $aauthor;
    }
    
    public function setPhylosophy($aphylosophy){
        $this->_aphylosophy = $aphylosophy;
    }
    
    public function toArray(){
        $obj = array();
        $obj['title'] = $this->getTitle();
        $obj['description'] = $this->getDescription();
        $obj['date'] = $this->getDate();
        $obj['author'] = $this->getAuthor();
        $obj['phylosophy'] = $this->getPhylosophy();
        $obj['mediatype'] = W_TYPE_NULL;
        if($this->getAid() != NULL){
            $obj['aid'] = $this->getAid();
        }
        
        return $obj;
    }
}