<?php 

require_once(__DIR__.'/Article.php');

class ArticleYT extends Article{
    
    private $_yt;
    
    public function __construct($tit, $desc, $dat, $aut, $pylo, $yt, $aid = null){

        $this->setTitle($tit);
        $this->setDescription($desc);
        $this->setDate($dat);
        $this->setAuthor($aut);
        $this->setPhylosophy($pylo);
        $this->setYT($yt);
        $this->setAid($aid);
    }
    
    public function getType(){
        return W_TYPE_YT;
    }
    
    public function getYT(){
        return $this->_yt;
    }

    public function setYT($_yt){
        $this->_yt = $_yt;
    }

    public function getView(){
        return __DIR__.'/../inc/ArticleYT.php';
    }
    
    public function toArray(){
        $obj = array();
        $obj['title'] = $this->getTitle();
        $obj['description'] = $this->getDescription();
        $obj['date'] = $this->getDate();
        $obj['author'] = $this->getAuthor();
        $obj['phylosophy'] = $this->getPhylosophy();
        $obj['media2'] = $this->getYT();
        $obj['mediatype'] = W_TYPE_YT;
        if($this->getAid() != NULL){
            $obj['aid'] = $this->getAid();
        }
        
        return $obj;
    }
    
}