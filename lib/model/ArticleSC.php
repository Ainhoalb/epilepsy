<?php 

require_once(__DIR__.'/Article.php');

class ArticleSC extends Article{
    
    private $_scid;
   
    public function __construct($tit, $desc, $dat, $aut, $pylo, $sc, $aid = null){
        
        $this->setTitle($tit);
        $this->setDescription($desc);
        $this->setDate($dat);
        $this->setAuthor($aut);
        $this->setPhylosophy($pylo);
        $this->setScid($sc);
        $this->setAid($aid);
    }
    
    public function getType(){
        return W_TYPE_SC;
    }
    
    public function getScid(){
        return $this->_scid;
    }

    public function setScid($_scid){
        $this->_scid = $_scid;
    }

    public function getView(){
        return __DIR__.'/../inc/ArticleSC.php';
    }
   
    public function toArray(){
        
        $obj = array();
        $obj['title'] = $this->getTitle();
        $obj['description'] = $this->getDescription();
        $obj['date'] = $this->getDate();
        $obj['author'] = $this->getAuthor();
        $obj['phylosophy'] = $this->getPhylosophy();
        $obj['media3'] = $this->getScid();
        $obj['mediatype'] = W_TYPE_SC;
        if($this->getAid() != NULL){
            $obj['aid'] = $this->getAid();
        }
        
        return $obj;
    }
}